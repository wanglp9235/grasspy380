/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(stringlib_expandtabs__doc__,
"expandtabs($self, /, tabsize=8)\n"
"--\n"
"\n"
"Return a copy where all tab characters are expanded using spaces.\n"
"\n"
"If tabsize is not given, a tab size of 8 characters is assumed.");

#define STRINGLIB_EXPANDTABS_METHODDEF    \
    {"expandtabs", (PyCFunction)(void(*)(void))stringlib_expandtabs, METH_FASTCALL|METH_KEYWORDS, stringlib_expandtabs__doc__},

PyDoc_STRVAR(stringlib_展开tab__doc__,
"展开tab($self, /, tabsize=8)\n"
"--\n"
"\n"
"Return a copy where all tab characters are expanded using spaces.\n"
"\n"
"If tabsize is not given, a tab size of 8 characters is assumed.");

#define STRINGLIB_展开tab_METHODDEF    \
    {"展开tab", (PyCFunction)(void(*)(void))stringlib_expandtabs, METH_FASTCALL|METH_KEYWORDS, stringlib_展开tab__doc__},

static PyObject *
stringlib_expandtabs_impl(PyObject *self, int tabsize);

static PyObject *
stringlib_expandtabs(PyObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"tabsize", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "expandtabs", 0};
    PyObject *argsbuf[1];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    int tabsize = 8;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    tabsize = _PyLong_AsInt(args[0]);
    if (tabsize == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_pos:
    return_value = stringlib_expandtabs_impl(self, tabsize);

exit:
    return return_value;
}

PyDoc_STRVAR(stringlib_ljust__doc__,
"ljust($self, width, fillchar=b\' \', /)\n"
"--\n"
"\n"
"Return a left-justified string of length width.\n"
"\n"
"Padding is done using the specified fill character.");

#define STRINGLIB_LJUST_METHODDEF    \
    {"ljust", (PyCFunction)(void(*)(void))stringlib_ljust, METH_FASTCALL, stringlib_ljust__doc__},

PyDoc_STRVAR(stringlib_左对齐__doc__,
"左对齐($self, 宽度, 填充字符=b\' \', /)\n"
"--\n"
"\n"
"返回长为<宽度>的左对齐字符串.\n"
"\n"
"用指定填充字符填充.");

#define STRINGLIB_左对齐_METHODDEF    \
    {"左对齐", (PyCFunction)(void(*)(void))stringlib_ljust, METH_FASTCALL, stringlib_左对齐__doc__},

static PyObject *
stringlib_ljust_impl(PyObject *self, Py_ssize_t width, char fillchar);

static PyObject *
stringlib_ljust(PyObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t width;
    char fillchar = ' ';

    if (!_PyArg_CheckPositional("ljust", nargs, 1, 2)) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        width = ival;
    }
    if (nargs < 2) {
        goto skip_optional;
    }
    if (PyBytes_Check(args[1]) && PyBytes_GET_SIZE(args[1]) == 1) {
        fillchar = PyBytes_AS_STRING(args[1])[0];
    }
    else if (PyByteArray_Check(args[1]) && PyByteArray_GET_SIZE(args[1]) == 1) {
        fillchar = PyByteArray_AS_STRING(args[1])[0];
    }
    else {
        _PyArg_BadArgument("ljust", "参数 2", "长为 1 的字节串", args[1]);
        goto exit;
    }
skip_optional:
    return_value = stringlib_ljust_impl(self, width, fillchar);

exit:
    return return_value;
}

PyDoc_STRVAR(stringlib_rjust__doc__,
"rjust($self, width, fillchar=b\' \', /)\n"
"--\n"
"\n"
"Return a right-justified string of length width.\n"
"\n"
"Padding is done using the specified fill character.");

#define STRINGLIB_RJUST_METHODDEF    \
    {"rjust", (PyCFunction)(void(*)(void))stringlib_rjust, METH_FASTCALL, stringlib_rjust__doc__},

PyDoc_STRVAR(stringlib_右对齐__doc__,
"右对齐($self, 宽度, 填充字符=b\' \', /)\n"
"--\n"
"\n"
"Return a right-justified string of length width.\n"
"\n"
"Padding is done using the specified fill character.");

#define STRINGLIB_右对齐_METHODDEF    \
    {"右对齐", (PyCFunction)(void(*)(void))stringlib_rjust, METH_FASTCALL, stringlib_右对齐__doc__},

static PyObject *
stringlib_rjust_impl(PyObject *self, Py_ssize_t width, char fillchar);

static PyObject *
stringlib_rjust(PyObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t width;
    char fillchar = ' ';

    if (!_PyArg_CheckPositional("rjust", nargs, 1, 2)) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        width = ival;
    }
    if (nargs < 2) {
        goto skip_optional;
    }
    if (PyBytes_Check(args[1]) && PyBytes_GET_SIZE(args[1]) == 1) {
        fillchar = PyBytes_AS_STRING(args[1])[0];
    }
    else if (PyByteArray_Check(args[1]) && PyByteArray_GET_SIZE(args[1]) == 1) {
        fillchar = PyByteArray_AS_STRING(args[1])[0];
    }
    else {
        _PyArg_BadArgument("rjust", "参数 2", "长为 1 的字节串", args[1]);
        goto exit;
    }
skip_optional:
    return_value = stringlib_rjust_impl(self, width, fillchar);

exit:
    return return_value;
}

PyDoc_STRVAR(stringlib_center__doc__,
"center($self, width, fillchar=b\' \', /)\n"
"--\n"
"\n"
"Return a centered string of length width.\n"
"\n"
"Padding is done using the specified fill character.");

#define STRINGLIB_CENTER_METHODDEF    \
    {"center", (PyCFunction)(void(*)(void))stringlib_center, METH_FASTCALL, stringlib_center__doc__},

PyDoc_STRVAR(stringlib_居中__doc__,
"居中($self, 宽度, 填充符=b\' \', /)\n"
"--\n"
"\n"
"Return a centered string of length width.\n"
"\n"
"Padding is done using the specified fill character.");

#define STRINGLIB_居中_METHODDEF    \
    {"居中", (PyCFunction)(void(*)(void))stringlib_center, METH_FASTCALL, stringlib_居中__doc__},

static PyObject *
stringlib_center_impl(PyObject *self, Py_ssize_t width, char fillchar);

static PyObject *
stringlib_center(PyObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t width;
    char fillchar = ' ';

    if (!_PyArg_CheckPositional("center", nargs, 1, 2)) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        width = ival;
    }
    if (nargs < 2) {
        goto skip_optional;
    }
    if (PyBytes_Check(args[1]) && PyBytes_GET_SIZE(args[1]) == 1) {
        fillchar = PyBytes_AS_STRING(args[1])[0];
    }
    else if (PyByteArray_Check(args[1]) && PyByteArray_GET_SIZE(args[1]) == 1) {
        fillchar = PyByteArray_AS_STRING(args[1])[0];
    }
    else {
        _PyArg_BadArgument("center", "参数 2", "长为 1 的字节串", args[1]);
        goto exit;
    }
skip_optional:
    return_value = stringlib_center_impl(self, width, fillchar);

exit:
    return return_value;
}

PyDoc_STRVAR(stringlib_zfill__doc__,
"zfill($self, width, /)\n"
"--\n"
"\n"
"Pad a numeric string with zeros on the left, to fill a field of the given width.\n"
"\n"
"The original string is never truncated.");

#define STRINGLIB_ZFILL_METHODDEF    \
    {"zfill", (PyCFunction)stringlib_zfill, METH_O, stringlib_zfill__doc__},

PyDoc_STRVAR(stringlib_填零__doc__,
"填零($self, 宽度, /)\n"
"--\n"
"\n"
"Pad a numeric string with zeros on the left, to fill a field of the given width.\n"
"\n"
"The original string is never truncated.");

#define STRINGLIB_填零_METHODDEF    \
    {"填零", (PyCFunction)stringlib_zfill, METH_O, stringlib_填零__doc__},

static PyObject *
stringlib_zfill_impl(PyObject *self, Py_ssize_t width);

static PyObject *
stringlib_zfill(PyObject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    Py_ssize_t width;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(arg);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        width = ival;
    }
    return_value = stringlib_zfill_impl(self, width);

exit:
    return return_value;
}
/*[clinic end generated code: output=15be047aef999b4e input=a9049054013a1b77]*/
