/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(list_insert__doc__,
"insert($self, index, object, /)\n"
"--\n"
"\n"
"Insert object before index.");

PyDoc_STRVAR(list_插入__doc__,
"插入($self, 索引, 对象, /)\n"
"--\n"
"\n"
"将对象插入在索引之前。");

#define LIST_INSERT_METHODDEF    \
    {"insert", (PyCFunction)(void(*)(void))list_insert, METH_FASTCALL, list_insert__doc__},

#define LIST_插入_METHODDEF    \
    {"插入", (PyCFunction)(void(*)(void))list_insert, METH_FASTCALL, list_插入__doc__},

static PyObject *
list_insert_impl(PyListObject *self, Py_ssize_t index, PyObject *object);

static PyObject *
list_insert(PyListObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t index;
    PyObject *object;

    if (!_PyArg_CheckPositional("insert", nargs, 2, 2)) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        index = ival;
    }
    object = args[1];
    return_value = list_insert_impl(self, index, object);

exit:
    return return_value;
}

PyDoc_STRVAR(list_clear__doc__,
"clear($self, /)\n"
"--\n"
"\n"
"Remove all items from list.");

#define LIST_CLEAR_METHODDEF    \
    {"clear", (PyCFunction)list_clear, METH_NOARGS, list_clear__doc__},

PyDoc_STRVAR(list_清空__doc__,
"清空($self, /)\n"
"--\n"
"\n"
"清除列表中的所有元素。");

#define LIST_清空_METHODDEF    \
    {"清空", (PyCFunction)list_clear, METH_NOARGS, list_清空__doc__},

static PyObject *
list_clear_impl(PyListObject *self);

static PyObject *
list_clear(PyListObject *self, PyObject *Py_UNUSED(ignored))
{
    return list_clear_impl(self);
}

PyDoc_STRVAR(list_copy__doc__,
"copy($self, /)\n"
"--\n"
"\n"
"Return a shallow copy of the list.");

#define LIST_COPY_METHODDEF    \
    {"copy", (PyCFunction)list_copy, METH_NOARGS, list_copy__doc__},

PyDoc_STRVAR(list_复制__doc__,
"复制($self, /)\n"
"--\n"
"\n"
"返回列表的浅层拷贝。");

#define LIST_复制_METHODDEF    \
    {"复制", (PyCFunction)list_copy, METH_NOARGS, list_复制__doc__},

static PyObject *
list_copy_impl(PyListObject *self);

static PyObject *
list_copy(PyListObject *self, PyObject *Py_UNUSED(ignored))
{
    return list_copy_impl(self);
}

PyDoc_STRVAR(list_append__doc__,
"append($self, object, /)\n"
"--\n"
"\n"
"Append object to the end of the list.");

PyDoc_STRVAR(list_追加__doc__,
"追加($self, 对象, /)\n"
"--\n"
"\n"
"将对象追加到对象末尾。");

#define LIST_APPEND_METHODDEF    \
    {"append", (PyCFunction)list_append, METH_O, list_append__doc__},

#define LIST_追加_METHODDEF    \
    {"追加", (PyCFunction)list_append, METH_O, list_追加__doc__},

PyDoc_STRVAR(list_extend__doc__,
"extend($self, iterable, /)\n"
"--\n"
"\n"
"Extend list by appending elements from the iterable.");

#define LIST_EXTEND_METHODDEF    \
    {"extend", (PyCFunction)list_extend, METH_O, list_extend__doc__},

PyDoc_STRVAR(list_扩充__doc__,
"扩充($self, 可迭代对象, /)\n"
"--\n"
"\n"
"通过追加可迭代对象中的元素来扩充列表。");

#define LIST_扩充_METHODDEF    \
    {"扩充", (PyCFunction)list_extend, METH_O, list_扩充__doc__},

PyDoc_STRVAR(list_pop__doc__,
"pop($self, index=-1, /)\n"
"--\n"
"\n"
"Remove and return item at index (default last).\n"
"\n"
"Raises IndexError if list is empty or index is out of range.");

#define LIST_POP_METHODDEF    \
    {"pop", (PyCFunction)(void(*)(void))list_pop, METH_FASTCALL, list_pop__doc__},

PyDoc_STRVAR(list_弹出__doc__,
"弹出($self, 索引=-1, /)\n"
"--\n"
"\n"
"移除并返回索引位置的元素（默认为最后一个元素）。\n"
"\n"
"Raises IndexError if list is empty or index is out of range.");

#define LIST_弹出_METHODDEF    \
    {"弹出", (PyCFunction)(void(*)(void))list_pop, METH_FASTCALL, list_弹出__doc__},

static PyObject *
list_pop_impl(PyListObject *self, Py_ssize_t index);

static PyObject *
list_pop(PyListObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t index = -1;

    if (!_PyArg_CheckPositional("pop", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        index = ival;
    }
skip_optional:
    return_value = list_pop_impl(self, index);

exit:
    return return_value;
}

PyDoc_STRVAR(list_sort__doc__,
"sort($self, /, *, key=None, reverse=False)\n"
"--\n"
"\n"
"Sort the list in ascending order and return None.\n"
"\n"
"The sort is in-place (i.e. the list itself is modified) and stable (i.e. the\n"
"order of two equal elements is maintained).\n"
"\n"
"If a key function is given, apply it once to each list item and sort them,\n"
"ascending or descending, according to their function values.\n"
"\n"
"The reverse flag can be set to sort in descending order.");

#define LIST_SORT_METHODDEF    \
    {"sort", (PyCFunction)(void(*)(void))list_sort, METH_FASTCALL|METH_KEYWORDS, list_sort__doc__},

PyDoc_STRVAR(list_排序__doc__,
"排序($self, /, *, 键=空, 逆=假)\n"
"--\n"
"\n"
"按升序对列表排序，返回空。\n"
"\n"
"排序在原地进行（即列表本身被修改），且为稳定排序（即\n"
"两个相同元素的顺序保持不变）。\n"
"\n"
"If a key function is given, apply it once to each list item and sort them,\n"
"ascending or descending, according to their function values.\n"
"\n"
"The reverse flag can be set to sort in descending order.");

#define LIST_排序_METHODDEF    \
    {"排序", (PyCFunction)(void(*)(void))list_排序, METH_FASTCALL|METH_KEYWORDS, list_排序__doc__},

static PyObject *
list_sort_impl(PyListObject *self, PyObject *keyfunc, int reverse);

static PyObject *
list_sort(PyListObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"key", "reverse", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "sort", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *keyfunc = Py_None;
    int reverse = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 0, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    if (args[0]) {
        keyfunc = args[0];
        if (!--noptargs) {
            goto skip_optional_kwonly;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "integer argument expected, got float" );
        goto exit;
    }
    reverse = _PyLong_AsInt(args[1]);
    if (reverse == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = list_sort_impl(self, keyfunc, reverse);

exit:
    return return_value;
}

static PyObject *
list_排序(PyListObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"键", "逆", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "排序", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *keyfunc = Py_None;
    int reverse = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 0, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    if (args[0]) {
        keyfunc = args[0];
        if (!--noptargs) {
            goto skip_optional_kwonly;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    reverse = _PyLong_AsInt(args[1]);
    if (reverse == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = list_sort_impl(self, keyfunc, reverse);

exit:
    return return_value;
}

PyDoc_STRVAR(list_reverse__doc__,
"reverse($self, /)\n"
"--\n"
"\n"
"Reverse *IN PLACE*.");

#define LIST_REVERSE_METHODDEF    \
    {"reverse", (PyCFunction)list_reverse, METH_NOARGS, list_reverse__doc__},

PyDoc_STRVAR(list_反转__doc__,
"反转($self, /)\n"
"--\n"
"\n"
"*原地*反转。");

#define LIST_反转_METHODDEF    \
    {"反转", (PyCFunction)list_reverse, METH_NOARGS, list_反转__doc__},

static PyObject *
list_reverse_impl(PyListObject *self);

static PyObject *
list_reverse(PyListObject *self, PyObject *Py_UNUSED(ignored))
{
    return list_reverse_impl(self);
}

PyDoc_STRVAR(list_index__doc__,
"index($self, value, start=0, stop=sys.maxsize, /)\n"
"--\n"
"\n"
"Return first index of value.\n"
"\n"
"Raises ValueError if the value is not present.");

#define LIST_INDEX_METHODDEF    \
    {"index", (PyCFunction)(void(*)(void))list_index, METH_FASTCALL, list_index__doc__},

PyDoc_STRVAR(list_索引__doc__,
"索引($self, 值, 起=0, 止=sys.maxsize, /)\n"
"--\n"
"\n"
"返回指定值的第一个索引。\n"
"\n"
"Raises ValueError if the value is not present.");

#define LIST_索引_METHODDEF    \
    {"索引", (PyCFunction)(void(*)(void))list_index, METH_FASTCALL, list_索引__doc__},

static PyObject *
list_index_impl(PyListObject *self, PyObject *value, Py_ssize_t start,
                Py_ssize_t stop);

static PyObject *
list_index(PyListObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *value;
    Py_ssize_t start = 0;
    Py_ssize_t stop = PY_SSIZE_T_MAX;

    if (!_PyArg_CheckPositional("index", nargs, 1, 3)) {
        goto exit;
    }
    value = args[0];
    if (nargs < 2) {
        goto skip_optional;
    }
    if (!_PyEval_SliceIndexNotNone(args[1], &start)) {
        goto exit;
    }
    if (nargs < 3) {
        goto skip_optional;
    }
    if (!_PyEval_SliceIndexNotNone(args[2], &stop)) {
        goto exit;
    }
skip_optional:
    return_value = list_index_impl(self, value, start, stop);

exit:
    return return_value;
}

PyDoc_STRVAR(list_count__doc__,
"count($self, value, /)\n"
"--\n"
"\n"
"Return number of occurrences of value.");

#define LIST_COUNT_METHODDEF    \
    {"count", (PyCFunction)list_count, METH_O, list_count__doc__},

PyDoc_STRVAR(list_计数__doc__,
"计数($self, 值, /)\n"
"--\n"
"\n"
"返回指定值的出现次数。");

#define LIST_计数_METHODDEF    \
    {"计数", (PyCFunction)list_count, METH_O, list_计数__doc__},

PyDoc_STRVAR(list_remove__doc__,
"remove($self, value, /)\n"
"--\n"
"\n"
"Remove first occurrence of value.\n"
"\n"
"Raises ValueError if the value is not present.");

#define LIST_REMOVE_METHODDEF    \
    {"remove", (PyCFunction)list_remove, METH_O, list_remove__doc__},

PyDoc_STRVAR(list_移除__doc__,
"移除($self, 值, /)\n"
"--\n"
"\n"
"删除列表中指定值的第一个匹配项。\n"
"\n"
"若值不存在则报【值错误】。");

#define LIST_移除_METHODDEF    \
    {"移除", (PyCFunction)list_remove, METH_O, list_移除__doc__},

PyDoc_STRVAR(list___init____doc__,
"列表(可迭代对象=(), /)\n"
"--\n"
"\n"
"内置可变序列。\n"
"\n"
"If no argument is given, the constructor creates a new empty list.\n"
"The argument must be an iterable if specified.");

static int
list___init___impl(PyListObject *self, PyObject *iterable);

static int
list___init__(PyObject *self, PyObject *args, PyObject *kwargs)
{
    int return_value = -1;
    PyObject *iterable = NULL;

    if ((Py_TYPE(self) == &PyList_Type) &&
        !_PyArg_NoKeywords("list", kwargs)) {
        goto exit;
    }
    if (!_PyArg_CheckPositional("list", PyTuple_GET_SIZE(args), 0, 1)) {
        goto exit;
    }
    if (PyTuple_GET_SIZE(args) < 1) {
        goto skip_optional;
    }
    iterable = PyTuple_GET_ITEM(args, 0);
skip_optional:
    return_value = list___init___impl((PyListObject *)self, iterable);

exit:
    return return_value;
}

PyDoc_STRVAR(list___sizeof____doc__,
"__sizeof__($self, /)\n"
"--\n"
"\n"
"Return the size of the list in memory, in bytes.");

#define LIST___SIZEOF___METHODDEF    \
    {"__sizeof__", (PyCFunction)list___sizeof__, METH_NOARGS, list___sizeof____doc__},

static PyObject *
list___sizeof___impl(PyListObject *self);

static PyObject *
list___sizeof__(PyListObject *self, PyObject *Py_UNUSED(ignored))
{
    return list___sizeof___impl(self);
}

PyDoc_STRVAR(list___reversed____doc__,
"__reversed__($self, /)\n"
"--\n"
"\n"
"Return a reverse iterator over the list.");

#define LIST___REVERSED___METHODDEF    \
    {"__reversed__", (PyCFunction)list___reversed__, METH_NOARGS, list___reversed____doc__},

static PyObject *
list___reversed___impl(PyListObject *self);

static PyObject *
list___reversed__(PyListObject *self, PyObject *Py_UNUSED(ignored))
{
    return list___reversed___impl(self);
}
/*[clinic end generated code: output=73718c0c33798c62 input=a9049054013a1b77]*/
