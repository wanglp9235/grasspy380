'''本模块实现了一些特殊的容器，针对 Python 预置的容器数据类型，例如 列表、字典
和 元组，提供了另一种选择。

* 具名元组   创建命名元组子类的工厂函数
* 双端队列   类似列表(list)的容器，实现了在两端快速追加(append)和弹出(pop)
* 链式映射   类似字典(dict)的容器类，将多个映射集合到一个视图里面
* 计数器类   字典的子类，用于计数可哈希对象
* 有序字典   字典的子类，保存了元素被添加的顺序
* 默认字典   字典的子类，提供了一个工厂函数，为字典查询提供一个默认值
* 用户字典   封装的字典对象，简化了字典子类化
* 用户列表   封装的列表对象，简化了列表子类化
* 用户字符串 封装的字符串对象，简化了字符串子类化

'''

__all__ = ['双端队列', '默认字典', '具名元组', '用户字典', '用户列表',
            '用户字符串', '计数器类', '有序字典', '链式映射']

从 collections 导入 *
from typing import NamedTuple
################################################################################
### 有序字典
################################################################################

类 有序字典(OrderedDict):
    '能记住插入顺序的字典'
    # An inherited dict maps keys to values.
    # The inherited dict provides __getitem__, __len__, __contains__, and get.
    # The remaining methods are order-aware.
    # Big-O running times for all methods are the same as regular dictionaries.

    # The internal self.__map dict maps keys to links in a doubly linked list.
    # The circular doubly linked list starts and ends with a sentinel element.
    # The sentinel element never gets deleted (this simplifies the algorithm).
    # The sentinel is in self.__hardroot with a weakref proxy in self.__root.
    # The prev links are weakref proxies (to prevent circular references).
    # Individual links are kept alive by the hard reference in self.__map.
    # Those hard references disappear when a key is deleted from an OrderedDict.

    函 __init__(自身, 其他=(), /, **关键词参数):
        '''初始化有序字典. 签名与常规字典相同. 保留关键词参数顺序.
        '''
        super().__init__(其他, **关键词参数)

    函 清空(自身):
        '移除所有元素'
        自身.clear()

    函 弹出项(自身, 最后=真):
        '''从字典中移除并返回一个 (键, 值) 对.

        如果 '最后' 为真, 则以后进先出的顺序弹出, 否则以先进先出的顺序弹出.
        '''
        如果 非 自身:
            报 键错误类('字典为空')
        返回 自身.popitem(最后)

    函 移至端(自身, 键, 最后=真):
        '将现有元素移至末尾 (如果"最后"为假则是开头).'
        自身.move_to_end(键, 最后)

    更新 = OrderedDict.update

    函 键(自身):
        '返回一个集合类对象, 提供关于字典所有键的试图'
        返回 自身.keys()

    函 项(自身):
        '返回一个集合类对象, 提供关于字典所有项的试图'
        返回 自身.items()

    函 值(自身):
        '返回一个集合类对象, 提供关于字典所有值的试图'
        返回 自身.values()

    __marker = object()

    函 弹出(自身, 键, 默认值=__marker):
        '''移除指定键并返回对应的值. 如果键未找到,
        有默认值则返回默认值, 无默认值则抛出键错误异常.
        '''
        if 键 in 自身:
            result = 自身[键]
            del 自身[键]
            return result
        if 默认值 is 自身.__marker:
            raise KeyError(键)
        return 默认值

    函 设默认值(自身, 键, 默认值=空):
        '''如果 "键" 不在字典中, 则插入带默认值的键.
        如果 "键" 在字典中, 则返回对应的值, 否则返回默认值.
        '''
        返回 自身.setdefault(键, 默认值)

    函 复制(自身):
        '返回有序字典的浅表拷贝'
        返回 自身.copy()

    @classmethod
    函 从键创建(本类, 可迭代对象, 值=空) -> '有序字典':
        '''创建一个新的有序字典, 键来自可迭代对象, 值为给定的值'''
        返回 本类.fromkeys(可迭代对象, 值)
    

################################################################################
### 具名元组
################################################################################

函 具名元组(类型名称, 字段名称, *, 重命名=假, 默认值=空, 模块=空) -> NamedTuple:
    '生成可以使用名称来访问元素内容的元组子类'
    结果 = namedtuple(类型名称, 字段名称, rename=重命名, defaults=默认值, module=模块)
    结果._字段 = 结果._fields
    结果._字段默认值 = 结果._fields_defaults
    结果._生成 = 结果._make
    结果._替换 = 结果._replace
    结果._转字典 = 结果._asdict
    返回 结果


################################################################################
### 计数器类
################################################################################

类 计数器类(Counter):
    '字典子类, 用于统计元素的出现次数.'

    函 最常见(自身, n=空):
        '''列出出现次数最多的 n 个元素及其相应的次数.
        如果 n 为空, 则列出所有元素及其出现次数.
        ''' 
        返回 自身.most_common(n)

    函 所有元素(自身):
        '返回一个迭代器, 每个元素按其出现次数重复.'
        返回 自身.elements()

    函 更新(自身, 可迭代对象=空, /, **关键词参数):
        '类似字典的更新方法, 但次数是增加而非替换.'
        自身.update(可迭代对象, **关键词参数)

    函 减去(自身, 可迭代对象=空, /, **关键词参数):
        '类似字典的更新方法, 但次数是减少而非替换. 次数可以为 0 或负数.'
        自身.subtract(可迭代对象, **关键词参数)

    函 复制(自身):
        '返回一个浅表拷贝'
        返回 自身.copy()


########################################################################
###  链式映射
########################################################################

类 链式映射(ChainMap):
    '''将多个字典 (或其他映射) 组织在一起, 产生单个可更新的试图.

    各映射存储在一个列表中. 使用 *映射* 属性可以访问或更新列表.

    查找操作会依次搜索各映射, 直至找到所需的键. 写入/更新/删除操作
    则相反, 仅针对第一个映射进行.
    '''
    函 __init__(自身, *映射):
        自身.映射 = 自身.maps = 列表(映射) or [{}]

    函 获取(自身, 键, 默认值=空):
        返回 自身.get(键, 默认值)

    @classmethod
    函 从键创建(本类, 可迭代对象, *参数) -> '链式映射':
        '利用从可迭代对象创建的单个字典创建一个链式映射'
        返回 本类(dict.fromkeys(可迭代对象, *参数))

    函 复制(自身):
        返回 自身.copy()

    函 新子映射(自身, 映射=空):
        '''返回一个新的链式映射: 新映射在前, 先前的所有映射在后.
        如果未提供映射, 则使用空字典.
        '''
        返回 自身.new_child(映射)

    @属性
    函 父映射(自身):
        '返回一个由 *映射[1:]* 构成的新链式映射'
        返回 自身.parents

    函 弹出项(自身):
        '从 *映射[0]* 移除并返回一个元素对'
        返回 自身.popitem()

    函 弹出(自身, 键, *参数):
        '从 *映射[0]* 移除指定键并返回其值'
        返回 自身.弹出(键, *参数)

    函 清空(自身):
        '清空 *映射[0]*, 其余映射保持不变'
        自身.clear()


################################################################################
### 用户字典
################################################################################

类 用户字典(UserDict):

    函 复制(自身):
        返回 自身.copy()

    @类方法
    函 从键创建(本类, 可迭代对象, 值=空):
        d = 本类()
        for key in 可迭代对象:
            d[key] = 值
        返回 d

    
################################################################################
### 用户列表
################################################################################

类 用户列表(UserList):

    def __init__(self, initlist=None):
        self.data = []
        if initlist is not None:
            # XXX should this accept an arbitrary sequence?
            if type(initlist) == type(self.data):
                self.data[:] = initlist
            elif isinstance(initlist, UserList):
                self.data[:] = initlist.data[:]
            else:
                self.data = list(initlist)
        self.数据 = self.data
        
    def 追加(self, 元素): self.data.append(元素)
    def 插入(self, i, 元素): self.data.insert(i, 元素)
    def 弹出(self, i=-1): return self.data.pop(i)
    def 移除(self, 元素): self.data.remove(元素)
    def 清空(self): self.data.clear()
    def 复制(self): return self.__class__(self)
    def 计数(self, 元素): return self.data.count(元素)
    def 索引(self, 元素, *参数): return self.data.index(元素, *参数)
    def 反转(self): self.data.reverse()
    def 排序(self, /, *参数, **关键词参数): self.data.sort(*参数, **关键词参数)
    def 扩充(self, 其他):
        if isinstance(其他, UserList):
            self.data.extend(其他.data)
        else:
            self.data.extend(其他)


################################################################################
### 用户字符串
################################################################################
导入 sys 为 _sys
类 用户字符串(UserString):

    def __init__(self, 序列):
        if isinstance(序列, str):
            self.data = 序列
        elif isinstance(序列, UserString):
            self.data = 序列.data[:]
        else:
            self.data = str(序列)
        self.数据 = self.data

    def 首字母大写(self): return self.__class__(self.data.capitalize())
    def 极小写(self):
        return self.__class__(self.data.casefold())
    def 居中(self, 宽度, *参数):
        return self.__class__(self.data.center(宽度, *参数))
    def 计数(self, sub, start=0, end=_sys.maxsize):
        if isinstance(sub, UserString):
            sub = sub.data
        return self.data.count(sub, start, end)
    def 编码(self, encoding='utf-8', errors='strict'):
        encoding = 'utf-8' if encoding is None else encoding
        errors = 'strict' if errors is None else errors
        return self.data.encode(encoding, errors)
    def 结尾是(self, suffix, start=0, end=_sys.maxsize):
        return self.data.endswith(suffix, start, end)
    def 展开tab(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))
    def 查找(self, sub, start=0, end=_sys.maxsize):
        if isinstance(sub, UserString):
            sub = sub.data
        return self.data.find(sub, start, end)
    def 格式化(self, /, *args, **kwds):
        return self.data.format(*args, **kwds)
    def 格式化_映射(self, mapping):
        return self.data.format_map(mapping)
    def 索引(self, sub, start=0, end=_sys.maxsize):
        return self.data.index(sub, start, end)
    def 是字母(self): return self.data.isalpha()
    def 是字母数字(self): return self.data.isalnum()
    def 是ascii(self): return self.data.isascii()
    def 是十进制符(self): return self.data.isdecimal()
    def 是数码(self): return self.data.isdigit()
    def 是标识符(self): return self.data.isidentifier()
    def 是小写(self): return self.data.islower()
    def 是数字(self): return self.data.isnumeric()
    def 是可打印符(self): return self.data.isprintable()
    def 是空白(self): return self.data.isspace()
    def 是标题(self): return self.data.istitle()
    def 是大写(self): return self.data.isupper()
    def 连接(self, seq): return self.data.join(seq)
    def 左对齐(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))
    def 小写(self): return self.__class__(self.data.lower())
    def 左修剪(self, chars=None): return self.__class__(self.data.lstrip(chars))
    制转换表 = maketrans = str.maketrans
    def 划分(self, sep):
        return self.data.partition(sep)
    def 替换(self, old, new, maxsplit=-1):
        if isinstance(old, UserString):
            old = old.data
        if isinstance(new, UserString):
            new = new.data
        return self.__class__(self.data.replace(old, new, maxsplit))
    def 右查找(self, sub, start=0, end=_sys.maxsize):
        if isinstance(sub, UserString):
            sub = sub.data
        return self.data.rfind(sub, start, end)
    def 右索引(self, sub, start=0, end=_sys.maxsize):
        return self.data.rindex(sub, start, end)
    def 右对齐(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))
    def 右划分(self, sep):
        return self.data.rpartition(sep)
    def 右修剪(self, chars=None):
        return self.__class__(self.data.rstrip(chars))
    def 分割(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)
    def 右分割(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)
    def 分行(self, keepends=False): return self.data.splitlines(keepends)
    def 开头是(self, prefix, start=0, end=_sys.maxsize):
        return self.data.startswith(prefix, start, end)
    def 修剪(self, 字符=空): return self.__class__(self.data.strip(字符))
    def 大小写互换(self): return self.__class__(self.data.swapcase())
    def 标题(self): return self.__class__(self.data.title())
    def 转换(self, *参数):
        return self.__class__(self.data.translate(*参数))
    def 大写(self): return self.__class__(self.data.upper())
    def 零填充(self, 宽度): return self.__class__(self.data.zfill(宽度))


类 双端队列(deque):
    '''最大的好处就是实现了从队列头部快速增加和取出对象: 左弹出(), 左追加(),
    时间复杂度是 O(1)。列表也能支持从头部添加和取出对象, 但时间复杂度是 O(n).
    '''     

类 默认字典(defaultdict):
    """使用原生数据结构'字典'的时候，如果用 d[键] 这样的方式访问，当指定的'键'不存在时，
    会抛出'键错误'异常。但如果使用'默认字典'，只要你传入一个默认工厂方法，
    那么请求一个不存在的'键'时， 便会调用这个工厂方法, 使用其结果来作为这个'键'的默认值。
    """
    