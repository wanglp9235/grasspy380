"""哈希库模块 - 众多哈希函数通用接口.

new(name, data=b'', **kwargs) - returns a new hash object implementing the
                                given hash function; initializing the hash
                                using the given binary data.

Named constructor functions are also available, these are faster
than using new(name):

md5(), sha1(), sha224(), sha256(), sha384(), sha512(), blake2b(), blake2s(),
sha3_224, sha3_256, sha3_384, sha3_512, shake_128, and shake_256.

More algorithms may be available on your platform but the above are guaranteed
to exist.  See the algorithms_guaranteed and algorithms_available attributes
to find out what algorithm names can be passed to new().

NOTE: If you want the adler32 or crc32 hash functions they are available in
the zlib module.

Choose your hash function wisely.  Some have known collision weaknesses.
sha384 and sha512 will be slow on 32 bit platforms.

哈希对象具有如下方法:
 - 更新(数据): Update the hash object with the bytes in data. Repeated calls
                 are equivalent to a single call with the concatenation of all
                 the arguments.
 - 摘要():     Return the digest of the bytes passed to the update() method
                 so far as a bytes object.
 - 十六进制摘要():  Like digest() except the digest is returned as a string
                 of double length, containing only hexadecimal digits.
 - 复制():       Return a copy (clone) of the hash object. This can be used to
                 efficiently compute the digests of datas that share a common
                 initial substring.

For example, to obtain the digest of the byte string 'Nobody inspects the
spammish repetition':

    >>> 导入 哈希库
    >>> m = 哈希库.md5()
    >>> m.更新(b"Nobody inspects")
    >>> m.更新(b" the spammish repetition")
    >>> m.摘要()
    b'\\xbbd\\x9c\\x83\\xdd\\x1e\\xa5\\xc9\\xd9\\xde\\xc9\\xa1\\x8d\\xf0\\xff\\xe9'

More condensed:

    >>> 哈希库.sha224(b"Nobody inspects the spammish repetition").十六进制摘要()
    'a4337bc45a8fc544c03f52dc550cd6e1e87021bc896588bd79e901e2'

"""

从 hashlib 导入 *