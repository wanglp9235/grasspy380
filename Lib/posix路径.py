'''
Posix 路径名称常用操作.
勿直接导入本模块.
'''

从 posixpath 导入 *
从 通用路径 导入 *

当前目录 = '.'
父目录 = '..'
扩展名分隔符 = '.'
分隔符 = '/'
路径分隔符 = ':'
默认路径 = '/bin:/usr/bin'
备选分隔符 = 空
空设备路径 = '/dev/null'

函 规范小写(字符串):
    '''
    将路径名称中的所有字符转换为小写, 斜杠转换为反斜杠.
    '''
    返回 normcase(字符串)

函 是绝对(字符串):
    '''
    测试一个路径是否为绝对路径.
    '''
    返回 isabs(字符串)

函 连接(路径, *其他路径):
    '''
    连接两个或更多路径.
    '''
    返回 join(路径, *其他路径)

函 分离磁盘(路径):
    '''
    将一个路径名称分离成磁盘 (或 UNC) 和相对路径两部分, 返回一个 2 元组.\n
    例如:\n
      分离盘符("c:/dir") -> ("c:", "/dir")
      分离盘符("//host/computer/dir") -> ("//host/computer", "/dir")
    '''
    返回 splitdrive(路径)

函 分离(路径):
    '''
    将一个路径名称分离成 (头, 尾) 两部分并返回, <尾> 为最后一个斜杠之后的所有内容.\n
    '''
    返回 split(路径)

函 分离文本(路径):
    '''
    将一个路径名称分离成 (根, 扩展名) 两部分并返回.\n
    '''
    返回 splitext(路径)

函 基本名称(路径):
    '''
    返回一个路径名称的最后一个组成部分.\n
    '''
    返回 basename(路径)

函 目录名称(路径):
    '''
    返回一个路径名称的目录部分.\n
    '''
    返回 dirname(路径)

函 是链接(路径):
    '''
    测试一个路径是否为符号链接.\n
    '''
    返回 islink(路径)

函 存在否l(路径):
    '''
    测试一个路径是否存在. 对于损坏的符号链接, 返回真.\n
    函数名末尾的 l 可理解为针对 link (链接) 的.
    '''
    返回 lexists(路径)

函 是挂载点(路径):
    '''
    测试一个路径是否为挂载点 (磁盘根、共享根或挂载的卷).
    '''
    返回 ismount(路径)

函 展开用户(路径):
    '''
    展开 ~ 和 ~user 结构.
    '''
    返回 expanduser(路径)

函 展开变量(路径):
    '''
    展开 $var / ${var} / %var% 形式的 shell 变量.
    '''
    返回 expanduser(路径)

函 规范路径(路径):
    '''
    对路径进行规范化处理, 比如消除双斜杠等.
    '''
    返回 normpath(路径)

函 绝对路径(路径):
    '''
    返回一个路径的绝对版本.
    '''
    返回 abspath(路径)

函 真实路径(路径):
    '''
    返回指定文件名的标准绝对路径, 消除路径中的所有符号链接.
    '''
    返回 realpath(路径)

支持统一码文件名 = supports_unicode_filenames

函 相对路径(路径):
    '''
    返回一个路径的相对版本.
    '''
    返回 relpath(路径)

函 共同路径(路径序列):
    '''
    给定一个路径名称序列, 返回最长共同子路径.
    '''
    返回 commonpath(路径序列)

