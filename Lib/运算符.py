"""
运算符接口

本模块提供一组对应于 Python 固有运算符的函数. 例如: 运算符.加(x, y) 相当于
表达式 x+y.

This module exports a set of functions corresponding to the intrinsic
operators of Python.  For example, operator.add(x, y) is equivalent
to the expression x+y.  The function names are those used for special
methods; variants without leading and trailing '__' are also provided
for convenience.

This is the pure Python implementation of the module.
"""
'''
__all__ = ['abs', 'add', 'and_', 'attrgetter', 'concat', 'contains', 'countOf',
           'delitem', 'eq', 'floordiv', 'ge', 'getitem', 'gt', 'iadd', 'iand',
           'iconcat', 'ifloordiv', 'ilshift', 'imatmul', 'imod', 'imul',
           'index', 'indexOf', 'inv', 'invert', 'ior', 'ipow', 'irshift',
           'is_', 'is_not', 'isub', 'itemgetter', 'itruediv', 'ixor', 'le',
           'length_hint', 'lshift', 'lt', 'matmul', 'methodcaller', 'mod',
           'mul', 'ne', 'neg', 'not_', 'or_', 'pos', 'pow', 'rshift',
           'setitem', 'sub', 'truediv', 'truth', 'xor']
'''
from builtins import abs as _abs


# Comparison Operations *******************************************************#

def 小于(a, b):
    "同 a < b."
    return a < b

def 小等(a, b):
    "同 a <= b."
    return a <= b

def 等于(a, b):
    "同 a == b."
    return a == b

def 不等(a, b):
    "同 a != b."
    return a != b

def 大等(a, b):
    "同 a >= b."
    return a >= b

def 大于(a, b):
    "同 a > b."
    return a > b

# Logical Operations **********************************************************#

def 非_(a):
    "同 非 a."
    return not a

def 真_(a):
    "如果 a 为真则返回 真, 否则返回 假."
    return True if a else False

def 是_(a, b):
    "同 a 是 b."
    return a is b

def 不是_(a, b):
    "同 a 不是 b."
    return a is not b

# Mathematical/Bitwise Operations *********************************************#

def 绝对值(a):
    "同 abs(a)."
    return _abs(a)

def 加(a, b):
    "同 a + b."
    return a + b

def 与(a, b):
    "同 a & b."
    return a & b

def 下整除(a, b): # floordiv
    "同 a // b."
    return a // b

def 索引(a):
    "同 a.__index__()."
    return a.__index__()

def 逆(a):
    "同 ~a."
    return ~a

# invert = inv

def 左移(a, b): # lshift
    "同 a << b."
    return a << b

def 模(a, b): # mod
    "同 a % b."
    return a % b

def 乘(a, b):
    "同 a * b."
    return a * b

def 矩阵乘(a, b): # matmul
    "同 a @ b."
    return a @ b

def 负(a):
    "同 -a."
    return -a

def 或_(a, b):
    "同 a | b."
    return a | b

def 正(a):
    "同 +a."
    return +a

def 幂(a, b):
    "同 a ** b."
    return a ** b

def 右移(a, b): # rshift
    "同 a >> b."
    return a >> b

def 减(a, b):
    "同 a - b."
    return a - b

def 除(a, b): # truediv 
    "同 a / b."
    return a / b

def 异或(a, b):
    "同 a ^ b."
    return a ^ b

# Sequence Operations *********************************************************#

def 连接(a, b): # concat
    "同 a + b, a 和 b 为序列."
    if not hasattr(a, '__getitem__'):
        msg = "'%s' 对象不支持连接" % type(a).__name__
        raise TypeError(msg)
    return a + b

def 包含(a, b): # contains
    "同 b 在 a (注意顺序)."
    return b in a

def 计数(a, b): # countof
    "返回 a 中 b 出现的次数."
    count = 0
    for i in a:
        if i == b:
            count += 1
    return count

def 删元素(a, b): # delitem
    "同 del a[b]."
    del a[b]

def 取元素(a, b):
    "同 a[b]."
    return a[b]

def 定位(a, b): # indexOf
    "返回 a 中 b 的第一个索引."
    for i, j in enumerate(a):
        if j == b:
            return i
    else:
        raise ValueError('序列.索引(x): x 不在序列中')

def 设元素(a, b, c):
    "同 a[b] = c."
    a[b] = c

def 长度提示(obj, default=0):
    """
    返回 obj 中元素数目的估计值.
    这对于从可迭代对象构建容器时预估容器大小很有用.

    如果对象支持 '长()' 函数, 则结果为确切值, 否则可能偏离任意值. 结果为 >= 0 的整数.
    """
    if not isinstance(default, int):
        msg = ("'%s' 对象无法被解读为整数" %
               type(default).__name__)
        raise TypeError(msg)

    try:
        return len(obj)
    except TypeError:
        pass

    try:
        hint = type(obj).__length_hint__
    except AttributeError:
        return default

    try:
        val = hint(obj)
    except TypeError:
        return default
    if val is NotImplemented:
        return default
    if not isinstance(val, int):
        msg = ('__length_hint__ 须为整数, 不能是 %s' %
               type(val).__name__)
        raise TypeError(msg)
    if val < 0:
        msg = '__length_hint__() 应返回 >= 0'
        raise ValueError(msg)
    return val

# Generalized Lookup Objects **************************************************#
# 这部分内容留待以后汉化

class attrgetter:
    """
    Return a callable object that fetches the given attribute(s) from its operand.
    After f = attrgetter('name'), the call f(r) returns r.name.
    After g = attrgetter('name', 'date'), the call g(r) returns (r.name, r.date).
    After h = attrgetter('name.first', 'name.last'), the call h(r) returns
    (r.name.first, r.name.last).
    """
    __slots__ = ('_attrs', '_call')

    def __init__(self, attr, *attrs):
        if not attrs:
            if not isinstance(attr, str):
                raise TypeError('attribute name must be a string')
            self._attrs = (attr,)
            names = attr.split('.')
            def func(obj):
                for name in names:
                    obj = getattr(obj, name)
                return obj
            self._call = func
        else:
            self._attrs = (attr,) + attrs
            getters = tuple(map(attrgetter, self._attrs))
            def func(obj):
                return tuple(getter(obj) for getter in getters)
            self._call = func

    def __call__(self, obj):
        return self._call(obj)

    def __repr__(self):
        return '%s.%s(%s)' % (self.__class__.__module__,
                              self.__class__.__qualname__,
                              ', '.join(map(repr, self._attrs)))

    def __reduce__(self):
        return self.__class__, self._attrs

class itemgetter:
    """
    Return a callable object that fetches the given item(s) from its operand.
    After f = itemgetter(2), the call f(r) returns r[2].
    After g = itemgetter(2, 5, 3), the call g(r) returns (r[2], r[5], r[3])
    """
    __slots__ = ('_items', '_call')

    def __init__(self, item, *items):
        if not items:
            self._items = (item,)
            def func(obj):
                return obj[item]
            self._call = func
        else:
            self._items = items = (item,) + items
            def func(obj):
                return tuple(obj[i] for i in items)
            self._call = func

    def __call__(self, obj):
        return self._call(obj)

    def __repr__(self):
        return '%s.%s(%s)' % (self.__class__.__module__,
                              self.__class__.__name__,
                              ', '.join(map(repr, self._items)))

    def __reduce__(self):
        return self.__class__, self._items

class methodcaller:
    """
    Return a callable object that calls the given method on its operand.
    After f = methodcaller('name'), the call f(r) returns r.name().
    After g = methodcaller('name', 'date', foo=1), the call g(r) returns
    r.name('date', foo=1).
    """
    __slots__ = ('_name', '_args', '_kwargs')

    def __init__(self, name, /, *args, **kwargs):
        self._name = name
        if not isinstance(self._name, str):
            raise TypeError('method name must be a string')
        self._args = args
        self._kwargs = kwargs

    def __call__(self, obj):
        return getattr(obj, self._name)(*self._args, **self._kwargs)

    def __repr__(self):
        args = [repr(self._name)]
        args.extend(map(repr, self._args))
        args.extend('%s=%r' % (k, v) for k, v in self._kwargs.items())
        return '%s.%s(%s)' % (self.__class__.__module__,
                              self.__class__.__name__,
                              ', '.join(args))

    def __reduce__(self):
        if not self._kwargs:
            return self.__class__, (self._name,) + self._args
        else:
            from functools import partial
            return partial(self.__class__, self._name, **self._kwargs), self._args


# In-place Operations *********************************************************#

def 自加(a, b):
    "同 a += b."
    a += b
    return a

def 自与(a, b):
    "同 a &= b."
    a &= b
    return a

def 自连接(a, b):
    "同 a += b, a 和 b 为序列."
    if not hasattr(a, '__getitem__'):
        msg = "'%s' 对象不支持连接" % type(a).__name__
        raise TypeError(msg)
    a += b
    return a

def 自下整除(a, b):
    "同 a //= b."
    a //= b
    return a

def 自左移(a, b):
    "同 a <<= b."
    a <<= b
    return a

def 自模(a, b):
    "同 a %= b."
    a %= b
    return a

def 自乘(a, b):
    "同 a *= b."
    a *= b
    return a

def 自矩阵乘(a, b):
    "同 a @= b."
    a @= b
    return a

def 自或(a, b):
    "同 a |= b."
    a |= b
    return a

def 自幂(a, b):
    "同 a **= b."
    a **=b
    return a

def 自右移(a, b):
    "同 a >>= b."
    a >>= b
    return a

def 自减(a, b):
    "同 a -= b."
    a -= b
    return a

def 自除(a, b):
    "同 a /= b."
    a /= b
    return a

def 自异或(a, b):
    "同 a ^= b."
    a ^= b
    return a

'''
try:
    from _operator import *
except ImportError:
    pass
else:
    from _operator import __doc__
'''
# All of these "__func__ = func" assignments have to happen after importing
# from _operator to make sure they're set to the right function
'''
__lt__ = lt
__le__ = le
__eq__ = eq
__ne__ = ne
__ge__ = ge
__gt__ = gt
__not__ = not_
__abs__ = abs
__add__ = add
__and__ = and_
__floordiv__ = floordiv
__index__ = index
__inv__ = inv
__invert__ = invert
__lshift__ = lshift
__mod__ = mod
__mul__ = mul
__matmul__ = matmul
__neg__ = neg
__or__ = or_
__pos__ = pos
__pow__ = pow
__rshift__ = rshift
__sub__ = sub
__truediv__ = truediv
__xor__ = xor
__concat__ = concat
__contains__ = contains
__delitem__ = delitem
__getitem__ = getitem
__setitem__ = setitem
__iadd__ = iadd
__iand__ = iand
__iconcat__ = iconcat
__ifloordiv__ = ifloordiv
__ilshift__ = ilshift
__imod__ = imod
__imul__ = imul
__imatmul__ = imatmul
__ior__ = ior
__ipow__ = ipow
__irshift__ = irshift
__isub__ = isub
__itruediv__ = itruediv
__ixor__ = ixor
'''