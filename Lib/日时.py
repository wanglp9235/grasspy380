"""本模块提供用于处理日期和的类。"""

# 时区相关部分有待进一步汉化和仔细测试

导入 datetime

最小年份 = 1
最大年份 = 9999

时间规格字典 = {
    '自动'   : 'auto',
    '小时数' : 'hours',
    '分钟数' : 'minutes',
    '秒数'   : 'seconds',
    '毫秒数' : 'milliseconds',
    '微秒数' : 'microseconds'
}

def _divide_and_round(a, b):
    """divide a by b and round result to the nearest integer

    When the ratio is exactly half-way between two integers,
    the even integer is returned.
    """
    # Based on the reference implementation for divmod_near
    # in Objects/longobject.c.
    q, r = divmod(a, b)
    # round up if either r / b > 0.5, or r / b == 0.5 and q is odd.
    # The expression r / b > 0.5 is equivalent to 2 * r > b if b is
    # positive, 2 * r < b if b negative.
    r *= 2
    greater_than_half = r > b if b > 0 else r < b
    if greater_than_half or r == b and q % 2 == 1:
        q += 1

    return q

类 时间差类(datetime.timedelta):
    """代表两个日期时间对象之差.

    支持的运算符:

    - 加/减一个时间差对象
    - 单目加/减/绝对值
    - 与另一个时间差对象比较
    - 乘/除一个整数

    此外, 两个日期时间对象相减得到一个时间差对象,
    日期时间对象加/减一个时间差得到一个日期时间对象.
    """

    函 __new__(本类, 天数=0, 秒数=0, 微秒数=0, 毫秒数=0, 分钟数=0, 小时数=0, 周数=0):
        返回 super().__new__(本类, days=天数, seconds=秒数, microseconds=微秒数,
                milliseconds=毫秒数, minutes=分钟数, hours=小时数, weeks=周数)

    def __repr__(self):
        args = []
        if self.days:
            args.append("天数=%d" % self.days)
        if self.seconds:
            args.append("秒数=%d" % self.seconds)
        if self.microseconds:
            args.append("微秒数=%d" % self.microseconds)
        if not args:
            args.append('0')
        return "%s.%s(%s)" % ('日时',
                              '时间差类',
                              ', '.join(args))

    def __str__(self):
        mm, ss = divmod(self.seconds, 60)
        hh, mm = divmod(mm, 60)
        s = "%d:%02d:%02d" % (hh, mm, ss)
        if self.days:
            s = ("%d 天, " % self.days) + s
        if self.microseconds:
            s = s + ".%06d" % self.microseconds
        return s

    函 总秒数(自身):
        返回 自身.total_seconds()

    @属性
    函 天数(自身):
        返回 自身.days
    
    @属性
    函 秒数(自身):
        返回 自身.seconds

    @属性
    函 微秒数(自身):
        返回 自身.microseconds

    def __add__(self, other):
        if isinstance(other, datetime.timedelta):
            # for CPython compatibility, we cannot use
            # our __class__ here, but need a real timedelta
            return 时间差类(self.days + other.days,
                             self.seconds + other.seconds,
                             self.microseconds + other.microseconds)
        return NotImplemented

    __radd__ = __add__

    def __sub__(self, other):
        if isinstance(other, datetime.timedelta):
            # for CPython compatibility, we cannot use
            # our __class__ here, but need a real timedelta
            return 时间差类(self.days - other.days,
                             self.seconds - other.seconds,
                             self.microseconds - other.microseconds)
        return NotImplemented

    def __rsub__(self, other):
        if isinstance(other, datetime.timedelta):
            return -self + other
        return NotImplemented

    def __neg__(self):
        # for CPython compatibility, we cannot use
        # our __class__ here, but need a real timedelta
        return 时间差类(-self.days,
                         -self.seconds,
                         -self.microseconds)

    def __pos__(self):
        return self

    def __abs__(self):
        if self.days < 0:
            return -self
        else:
            return self

    def __mul__(self, other):
        if isinstance(other, int):
            # for CPython compatibility, we cannot use
            # our __class__ here, but need a real timedelta
            return 时间差类(self.days * other,
                             self.seconds * other,
                             self.microseconds * other)
        if isinstance(other, float):
            usec = self._to_microseconds()
            a, b = other.as_integer_ratio()
            return 时间差类(0, 0, _divide_and_round(usec * a, b))
        return NotImplemented

    __rmul__ = __mul__

    def _to_microseconds(self):
        return ((self.days * (24*3600) + self.seconds) * 1000000 +
                self.microseconds)

    def __floordiv__(self, other):
        if not isinstance(other, (int, datetime.timedelta)):
            return NotImplemented
        usec = self._to_microseconds()
        if isinstance(other, datetime.timedelta):
            return usec // other._to_microseconds()
        if isinstance(other, int):
            return 时间差类(0, 0, usec // other)

    def __truediv__(self, other):
        if not isinstance(other, (int, float, datetime.timedelta)):
            return NotImplemented
        usec = self._to_microseconds()
        if isinstance(other, datetime.timedelta):
            return usec / other._to_microseconds()
        if isinstance(other, int):
            return 时间差类(0, 0, _divide_and_round(usec, other))
        if isinstance(other, float):
            a, b = other.as_integer_ratio()
            return 时间差类(0, 0, _divide_and_round(b * usec, a))

    def __mod__(self, other):
        if isinstance(other, datetime.timedelta):
            r = self._to_microseconds() % other._to_microseconds()
            return 时间差类(0, 0, r)
        return NotImplemented

    def __divmod__(self, other):
        if isinstance(other, datetime.timedelta):
            q, r = divmod(self._to_microseconds(),
                          other._to_microseconds())
            return q, 时间差类(0, 0, r)
        return NotImplemented

时间差类.最小值 = datetime.timedelta.min
时间差类.最大值 = datetime.timedelta.max
时间差类.分辨率 = datetime.timedelta.resolution


类 日期类(datetime.date):
    """构造函数:
    
    __new__()
    从时间戳()
    今日()
    从序数()

    方法:

    时间元组()
    转序数()
    周i()
    iso周i(), iso日历(), iso格式()
    转字符串()
    转格式串()

    属性 (只读):
    年, 月, 日
    """

    函 __new__(本类, 年, 月=空, 日=空):
        返回 super().__new__(本类, 年, month=月, day=日)

    @classmethod
    函 从时间戳(本类, 时间) -> '日期类':
        返回 本类.fromtimestamp(时间)

    @classmethod
    函 今日(本类) -> '日期类':
        返回 本类.today()

    @classmethod
    函 从序数(本类, n) -> '日期类':
        "n - 第 n 天. 1 年的 1 月 1 日为第 1 天."
        返回 本类.fromordinal(n)

    @classmethod
    函 从iso格式(本类, 日期字符串) -> '日期类':
        "从 日期类.iso格式() 的输出构建一个日期对象."
        返回 本类.fromisoformat(日期字符串)

    @classmethod
    函 从iso日历(本类, 年, 第几周, 周i) -> '日期类':
        "从 iso 年/周数/周i 构建一个日期对象. 日期.iso日历() 函数的反函数"
        返回 本类.fromisocalendar(年, 第几周, 周i)

    函 转字符串(自身):
        返回 自身.ctime()

    函 转格式串(自身, 格式):
        返回 自身.strftime(格式)

    函 iso格式(自身):
        返回 自身.isoformat()

    @property
    函 年(自身):
        返回 自身.year
    
    @property
    函 月(自身):
        返回 自身.month

    @property
    函 日(自身):
        返回 自身.day

    函 时间元组(自身):
        "返回与 时间.本地时元组() 兼容的本地时间元组"
        返回 自身.timetuple()

    函 转序数(自身):
        "该日期对象为第几天. 1 年的 1 月 1 日为第 1 天"
        返回 自身.toordinal()

    函 替换(自身, 年=空, 月=空, 日=空) -> '日期类':
        if 年 is None:
            年 = 自身.year
        if 月 is None:
            月 = 自身.month
        if 日 is None:
            日 = 自身.day
        return type(自身)(年, 月, 日)

    def __sub__(self, other):
        """Subtract two dates, or a date and a timedelta."""
        if isinstance(other, datetime.timedelta):
            return self + datetime.timedelta(-other.days)
        if isinstance(other, datetime.date):
            days1 = self.toordinal()
            days2 = other.toordinal()
            return 时间差类(days1 - days2)
        return NotImplemented

    函 周i(自身):
        "周一 == 0 ... 周日 == 6"
        返回 自身.weekday()

    函 iso周i(自身):
        "周一 == 1 ... 周日 == 7"
        返回 自身.isoweekday()

    函 iso日历(自身):
        "返回一个 3 元组: (年, 第几周, 周i)"
        返回 自身.isocalendar()

日期类.最小值 = 日期类(1, 1, 1)
日期类.最大值 = 日期类(9999, 12, 31)
日期类.分辨率 = 时间差类(天数=1)


类 时区信息类(datetime.tzinfo):

    函 从utc(自身, 日期时间):
        "UTC 日期时间 -> 本地日期时间"
        返回 自身.fromutc(日期时间)

类 时间类(datetime.time):
    """
    方法:

    转格式串()
    iso格式()
    utc偏移量()
    时区名称()
    夏令时()

    属性 (只读):
    时, 分, 秒, 微秒, 时区信息, 折叠
    """

    函 __new__(本类, 时=0, 分=0, 秒=0, 微秒=0, 时区信息=空, *, 折叠=0):
        返回 super().__new__(本类, hour=时, minute=分, second=秒,
                            microsecond=微秒, tzinfo=时区信息, fold=折叠)

    @property
    函 时(自身):
        返回 自身.hour

    @property
    函 分(自身):
        返回 自身.minute

    @property
    函 秒(自身):
        返回 自身.second

    @property
    函 微秒(自身):
        返回 自身.microsecond

    @property
    函 时区信息(自身):
        返回 自身.tzinfo

    @property
    函 折叠(自身):
        返回 自身.fold

    函 iso格式(自身, 时间规格='自动'):
        '''时间规格选项: 自动, 小时数, 分钟数, 秒数, 毫秒数, 微秒数'''
        时间规格 = 时间规格字典.获取(时间规格, 时间规格)
        返回 自身.isoformat(时间规格)

    @classmethod
    函 从iso格式(本类, 时间字符串) -> '时间类':
        "从 iso格式() 的输出构建一个时间对象"
        返回 本类.fromisoformat(时间字符串)

    函 转格式串(自身, 格式):
        返回 自身.strftime(格式)

    函 utc偏移量(自身):
        "将时区偏移量作为时间差对象返回"
        返回 自身.utcoffset()

    函 时区名称(自身):
        返回 自身.tzname()

    函 夏令时(自身):
        返回 自身.dst()

    函 替换(自身, 时=空, 分=空, 秒=空, 微秒=空, 时区信息=真, *, 折叠=空) -> '时间类':
        """使用指定的新值返回一个新的时间对象."""
        if 时 is None:
            时 = 自身.hour
        if 分 is None:
            分 = 自身.minute
        if 秒 is None:
            秒 = 自身.second
        if 微秒 is None:
            微秒 = 自身.microsecond
        if 时区信息 is True:
            时区信息 = 自身.tzinfo
        if 折叠 is None:
            折叠 = 自身.fold
        return type(自身)(时, 分, 秒, 微秒, 时区信息, 折叠=折叠)

时间类.最小值 = 时间类(0, 0, 0)
时间类.最大值 = 时间类(23, 59, 59, 999999)
时间类.分辨率 = 时间差类(微秒数=1)


类 日时类(datetime.datetime, 日期类):

    函 __new__(本类, 年, 月=空, 日=空, 时=0, 分=0, 秒=0, 微秒=0, 时区信息=空, *, 折叠=0):
        返回 datetime.datetime.__new__(本类, 年, month=月, day=日, hour=时, minute=分,
                second=秒, microsecond=微秒, tzinfo=时区信息, fold=折叠)

    @property
    函 时(自身):
        返回 自身.hour

    @property
    函 分(自身):
        返回 自身.minute

    @property
    函 秒(自身):
        返回 自身.second

    @property
    函 微秒(自身):
        返回 自身.microsecond

    @property
    函 时区信息(自身):
        返回 自身.tzinfo

    @property
    函 折叠(自身):
        返回 自身.fold

    @classmethod
    函 从时间戳(本类, 时间, 时区=空) -> '日时类':
        返回 本类.fromtimestamp(时间, tz=时区)

    @classmethod
    函 从时间戳utc(本类, 时间) -> '日时类':
        返回 本类.utcfromtimestamp(时间)

    @classmethod
    函 此刻(本类, 时区=空) -> '日时类':
        返回 本类.now(时区)

    @classmethod
    函 此刻utc(本类) -> '日时类':
        返回 本类.utcnow()

    @classmethod
    函 合并(本类, 日期, 时间, 时区信息=真) -> '日时类':
        "从给定的日期和时间构建一个日时对象"
        if not isinstance(日期, datetime.date):
            raise TypeError("日期参数须为日期类实例")
        if not isinstance(时间, datetime.time):
            raise TypeError("时间参数须为时间类实例")
        if 时区信息 is True:
            时区信息 = 时间.tzinfo
        return 本类(日期.year, 日期.month, 日期.day,
                   时间.hour, 时间.minute, 时间.second, 时间.microsecond,
                   时区信息, 折叠=时间.fold)

    @classmethod
    函 从iso格式(本类, 日期字符串) -> '日时类':
        返回 本类.fromisoformat(日期字符串)

    函 时间元组(自身):
        "返回与 时间.本地时元组() 兼容的本地时间元组"
        返回 自身.timetuple()

    函 时间戳(自身):
        返回 自身.timestamp()

    函 utc时间元组(自身):
        "返回与 时间.本地时元组() 兼容的 utc 时间元组"
        返回 自身.utctimetuple()

    函 日期(自身) -> '日期类':
        "返回日期部分"
        返回 日期类(自身.年, 自身.月, 自身.日)

    函 时间(自身) -> '时间类':
        "返回时间部分"
        返回 时间类(自身.时, 自身.分, 自身.秒, 自身.微秒, 折叠=自身.折叠)

    函 时间时区(自身) -> '时间类':
        "返回时间部分和时区信息"
        返回 时间类(自身.时, 自身.分, 自身.秒, 自身.微秒, 自身.时区信息, 折叠=自身.折叠)

    函 替换(自身, 年=空, 月=空, 日=空, 时=空, 分=空, 秒=空, 微秒=空, 时区信息=真, *, 折叠=空) -> '日时类':
        """使用指定的新值返回一个新的日时对象."""
        if 年 is None:
            年 = 自身.year
        if 月 is None:
            月 = 自身.month
        if 日 is None:
            日 = 自身.day
        if 时 is None:
            时 = 自身.hour
        if 分 is None:
            分 = 自身.minute
        if 秒 is None:
            秒 = 自身.second
        if 微秒 is None:
            微秒 = 自身.microsecond
        if 时区信息 is True:
            时区信息 = 自身.tzinfo
        if 折叠 is None:
            折叠 = 自身.fold
        return type(自身)(年, 月, 日, 时, 分, 秒, 微秒, 时区信息, 折叠=折叠)

    函 时区调整(自身, 时区=空):  # TODO
        返回 自身.astimezone(时区)

    函 转字符串(自身):
        返回 自身.ctime()

    函 iso格式(自身, 分隔符='T', 时间规格='自动'):
        '''时间规格选项: 自动, 小时数, 分钟数, 秒数, 毫秒数, 微秒数'''
        时间规格 = 时间规格字典.获取(时间规格, 时间规格)
        返回 自身.isoformat(sep=分隔符, timespec=时间规格)

    @classmethod
    函 从格式串(本类, 日期字符串, 格式) -> '日时类':
        返回 本类.strptime(日期字符串, 格式)

    函 utc偏移量(自身) -> '时间差类':
        返回 自身.utcoffset()

    函 时区名称(自身):
        返回 自身.tzname()

    函 夏令时(自身):
        返回 自身.dst()

    def __sub__(self, other):
        "Subtract two datetimes, or a datetime and a timedelta."
        if not isinstance(other, datetime.datetime):
            if isinstance(other, datetime.timedelta):
                return self + -other
            return NotImplemented

        days1 = self.toordinal()
        days2 = other.toordinal()
        secs1 = self.second + self.minute * 60 + self.hour * 3600
        secs2 = other.second + other.minute * 60 + other.hour * 3600
        base = 时间差类(days1 - days2,
                         secs1 - secs2,
                         self.microsecond - other.microsecond)
        if self.tzinfo is other.tzinfo:
            return base
        myoff = self.utcoffset()
        otoff = other.utcoffset()
        if myoff == otoff:
            return base
        if myoff is None or otoff is None:
            raise TypeError("不能混用简单型时间和时区感知型时间")
        return base + otoff - myoff

日时类.最小值 = 日时类(1, 1, 1)
日时类.最大值 = 日时类(9999, 12, 31, 23, 59, 59, 999999)
日时类.分辨率 = 时间差类(微秒数=1)


类 时区类(时区信息类):
    __slots__ = '_offset', '_name'

    _Omitted = object()
    def __new__(cls, 偏移量, 名称=_Omitted):
        if not isinstance(偏移量, datetime.timedelta):
            raise TypeError("偏移量须为时间差对象")
        if 名称 is cls._Omitted:
            if not 偏移量:
                return cls.utc
            名称 = None
        elif not isinstance(名称, str):
            raise TypeError("名称须为字符串")
        if not cls._minoffset <= 偏移量 <= cls._maxoffset:
            raise ValueError("偏移量须为时间差对象, "
                             "严格介于 -时间差类(小时数=24) 和 "
                             "时间差类(小时数=24) 之间.")
        return cls._create(偏移量, 名称)

    @classmethod
    def _create(cls, offset, name=None):
        self = 时区信息类.__new__(cls)
        self._offset = offset
        self._name = name
        return self

    def __getinitargs__(self):
        """pickle support"""
        if self._name is None:
            return (self._offset,)
        return (self._offset, self._name)

    def __eq__(self, other):
        if isinstance(other, datetime.timezone):
            return self._offset == other._offset
        return NotImplemented

    def __hash__(self):
        return hash(self._offset)

    def __repr__(self):
        """Convert to formal string, for repr().

        >>> tz = timezone.utc
        >>> repr(tz)
        'datetime.timezone.utc'
        >>> tz = timezone(timedelta(hours=-5), 'EST')
        >>> repr(tz)
        "datetime.timezone(datetime.timedelta(-1, 68400), 'EST')"
        """
        if self is self.utc:
            return '日时.时区类.utc'
        if self._name is None:
            return "%s.%s(%r)" % (self.__class__.__module__,
                                  self.__class__.__qualname__,
                                  self._offset)
        return "%s.%s(%r, %r)" % (self.__class__.__module__,
                                  self.__class__.__qualname__,
                                  self._offset, self._name)

    def __str__(self):
        return self.时区名称(None)

    函 utc偏移量(自身, 日期时间):
        if isinstance(日期时间, datetime.datetime) or 日期时间 is None:
            return 自身._offset
        raise TypeError("utc偏移量() 参数须为日时类实例"
                        "或为空")

    函 时区名称(自身, 日期时间):
        if isinstance(日期时间, datetime.datetime) or 日期时间 is None:
            if 自身._name is None:
                return 自身._name_from_offset(自身._offset)
            return 自身._name
        raise TypeError("时区名称() 参数须为日时类实例"
                        "或为空")

    函 夏令时(自身, 日期时间):
        if isinstance(日期时间, datetime.datetime) or 日期时间 is None:
            return None
        raise TypeError("夏令时() 参数须为日时类实例"
                        "或为空")

    函 从utc(自身, 日期时间):
        if isinstance(日期时间, datetime.datetime):
            if 日期时间.tzinfo is not 自身:
                raise ValueError("从utc: 日期时间.时区信息 "
                                 "不是自身")
            return 日期时间 + 自身._offset
        raise TypeError("从utc() 参数须为日时类实例"
                        "或为空")

    _maxoffset = 时间差类(小时数=24, 毫秒数=-1)
    _minoffset = -_maxoffset

    @staticmethod
    def _name_from_offset(delta):
        if not delta:
            return 'UTC'
        if delta < 时间差类(0):
            sign = '-'
            delta = -delta
        else:
            sign = '+'
        hours, rest = divmod(delta, 时间差类(小时数=1))
        minutes, rest = divmod(rest, 时间差类(分钟数=1))
        seconds = rest.seconds
        microseconds = rest.microseconds
        if microseconds:
            return (f'UTC{sign}{hours:02d}:{minutes:02d}:{seconds:02d}'
                    f'.{microseconds:06d}')
        if seconds:
            return f'UTC{sign}{hours:02d}:{minutes:02d}:{seconds:02d}'
        return f'UTC{sign}{hours:02d}:{minutes:02d}'

时区类.utc = datetime.timezone.utc
时区类.最小值 = datetime.timezone.min
时区类.最大值 = datetime.timezone.max
