/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(math_gcd__doc__,
"gcd($module, x, y, /)\n"
"--\n"
"\n"
"greatest common divisor of x and y");

#define MATH_GCD_METHODDEF    \
    {"gcd", (PyCFunction)(void(*)(void))math_gcd, METH_FASTCALL, math_gcd__doc__},

PyDoc_STRVAR(math_最大公约数__doc__,
"最大公约数($module, x, y, /)\n"
"--\n"
"\n"
"求 x 和 y 的最大公约数");

#define MATH_最大公约数_METHODDEF    \
    {"最大公约数", (PyCFunction)(void(*)(void))math_gcd, METH_FASTCALL, math_最大公约数__doc__},

static PyObject *
math_gcd_impl(PyObject *module, PyObject *a, PyObject *b);

static PyObject *
math_gcd(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *a;
    PyObject *b;

    if (!_PyArg_CheckPositional("gcd", nargs, 2, 2)) {
        goto exit;
    }
    a = args[0];
    b = args[1];
    return_value = math_gcd_impl(module, a, b);

exit:
    return return_value;
}

PyDoc_STRVAR(math_ceil__doc__,
"ceil($module, x, /)\n"
"--\n"
"\n"
"Return the ceiling of x as an Integral.\n"
"\n"
"This is the smallest integer >= x.");

#define MATH_CEIL_METHODDEF    \
    {"ceil", (PyCFunction)math_ceil, METH_O, math_ceil__doc__},

PyDoc_STRVAR(math_上取整__doc__,
"上取整($module, x, /)\n"
"--\n"
"\n"
"返回 x 向上取整的结果.\n"
"\n"
"结果是 >= x 的最小整数.");

#define MATH_上取整_METHODDEF    \
    {"上取整", (PyCFunction)math_ceil, METH_O, math_上取整__doc__},

PyDoc_STRVAR(math_floor__doc__,
"floor($module, x, /)\n"
"--\n"
"\n"
"Return the floor of x as an Integral.\n"
"\n"
"This is the largest integer <= x.");

#define MATH_FLOOR_METHODDEF    \
    {"floor", (PyCFunction)math_floor, METH_O, math_floor__doc__},

PyDoc_STRVAR(math_下取整__doc__,
"下取整($module, x, /)\n"
"--\n"
"\n"
"返回 x 向下取整的结果.\n"
"\n"
"结果是 <= x 的最大整数.");

#define MATH_下取整_METHODDEF    \
    {"下取整", (PyCFunction)math_floor, METH_O, math_下取整__doc__},

PyDoc_STRVAR(math_fsum__doc__,
"fsum($module, seq, /)\n"
"--\n"
"\n"
"Return an accurate floating point sum of values in the iterable seq.\n"
"\n"
"Assumes IEEE-754 floating point arithmetic.");

#define MATH_FSUM_METHODDEF    \
    {"fsum", (PyCFunction)math_fsum, METH_O, math_fsum__doc__},

PyDoc_STRVAR(math_浮点和__doc__,
"浮点和($module, 序列, /)\n"
"--\n"
"\n"
"返回可迭代序列中所有值的精确浮点和.\n"
"\n"
"采用 IEEE-754 浮点算术.");

#define MATH_浮点和_METHODDEF    \
    {"浮点和", (PyCFunction)math_fsum, METH_O, math_浮点和__doc__},

PyDoc_STRVAR(math_isqrt__doc__,
"isqrt($module, n, /)\n"
"--\n"
"\n"
"Return the integer part of the square root of the input.");

#define MATH_ISQRT_METHODDEF    \
    {"isqrt", (PyCFunction)math_isqrt, METH_O, math_isqrt__doc__},

PyDoc_STRVAR(math_开方取整__doc__,
"开方取整($module, n, /)\n"
"--\n"
"\n"
"返回输入的平方根的整数部分.");

#define MATH_开方取整_METHODDEF    \
    {"开方取整", (PyCFunction)math_isqrt, METH_O, math_开方取整__doc__},

PyDoc_STRVAR(math_factorial__doc__,
"factorial($module, x, /)\n"
"--\n"
"\n"
"Find x!.\n"
"\n"
"Raise a ValueError if x is negative or non-integral.");

#define MATH_FACTORIAL_METHODDEF    \
    {"factorial", (PyCFunction)math_factorial, METH_O, math_factorial__doc__},

PyDoc_STRVAR(math_阶乘__doc__,
"阶乘($module, x, /)\n"
"--\n"
"\n"
"求 x!。\n"
"\n"
"若 x 为负数或非整数则报【值错误】。");

#define MATH_阶乘_METHODDEF    \
    {"阶乘", (PyCFunction)math_factorial, METH_O, math_阶乘__doc__},

PyDoc_STRVAR(math_trunc__doc__,
"trunc($module, x, /)\n"
"--\n"
"\n"
"Truncates the Real x to the nearest Integral toward 0.\n"
"\n"
"Uses the __trunc__ magic method.");

#define MATH_TRUNC_METHODDEF    \
    {"trunc", (PyCFunction)math_trunc, METH_O, math_trunc__doc__},

PyDoc_STRVAR(math_截取__doc__,
"截取($module, x, /)\n"
"--\n"
"\n"
"返回实数 x 的整数部分.\n"
"\n"
"使用 __trunc__ 魔法方法.");

#define MATH_截取_METHODDEF    \
    {"截取", (PyCFunction)math_trunc, METH_O, math_截取__doc__},

PyDoc_STRVAR(math_frexp__doc__,
"frexp($module, x, /)\n"
"--\n"
"\n"
"Return the mantissa and exponent of x, as pair (m, e).\n"
"\n"
"m is a float and e is an int, such that x = m * 2.**e.\n"
"If x is 0, m and e are both 0.  Else 0.5 <= abs(m) < 1.0.");

#define MATH_FREXP_METHODDEF    \
    {"frexp", (PyCFunction)math_frexp, METH_O, math_frexp__doc__},

PyDoc_STRVAR(math_尾指__doc__,
"尾指($module, x, /)\n"
"--\n"
"\n"
"返回 x 的尾数和指数对 (m, e).\n"
"\n"
"m 为浮点数, e 为整数, 使得 x = m * 2.**e.\n"
"如果 x 为 0, 则 m 和 e 均为 0,  否则 0.5 <= 绝对值(m) < 1.0.");

#define MATH_尾指_METHODDEF    \
    {"尾指", (PyCFunction)math_frexp, METH_O, math_尾指__doc__},

static PyObject *
math_frexp_impl(PyObject *module, double x);

static PyObject *
math_frexp(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_frexp_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_ldexp__doc__,
"ldexp($module, x, i, /)\n"
"--\n"
"\n"
"Return x * (2**i).\n"
"\n"
"This is essentially the inverse of frexp().");

#define MATH_LDEXP_METHODDEF    \
    {"ldexp", (PyCFunction)(void(*)(void))math_ldexp, METH_FASTCALL, math_ldexp__doc__},

PyDoc_STRVAR(math_逆尾指__doc__,
"逆尾指($module, x, i, /)\n"
"--\n"
"\n"
"返回 x * (2**i).\n"
"\n"
"此函数本质上是 <尾指()> 的逆运算.");

#define MATH_逆尾指_METHODDEF    \
    {"逆尾指", (PyCFunction)(void(*)(void))math_ldexp, METH_FASTCALL, math_逆尾指__doc__},

static PyObject *
math_ldexp_impl(PyObject *module, double x, PyObject *i);

static PyObject *
math_ldexp(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    double x;
    PyObject *i;

    if (!_PyArg_CheckPositional("ldexp", nargs, 2, 2)) {
        goto exit;
    }
    if (PyFloat_CheckExact(args[0])) {
        x = PyFloat_AS_DOUBLE(args[0]);
    }
    else
    {
        x = PyFloat_AsDouble(args[0]);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    i = args[1];
    return_value = math_ldexp_impl(module, x, i);

exit:
    return return_value;
}

PyDoc_STRVAR(math_modf__doc__,
"modf($module, x, /)\n"
"--\n"
"\n"
"Return the fractional and integer parts of x.\n"
"\n"
"Both results carry the sign of x and are floats.");

#define MATH_MODF_METHODDEF    \
    {"modf", (PyCFunction)math_modf, METH_O, math_modf__doc__},

PyDoc_STRVAR(math_小整__doc__,
"小整($module, x, /)\n"
"--\n"
"\n"
"返回 x 的小数部分和整数部分.\n"
"\n"
"两部分的符号与 x 相同, 且均为浮点数.");

#define MATH_小整_METHODDEF    \
    {"小整", (PyCFunction)math_modf, METH_O, math_小整__doc__},

static PyObject *
math_modf_impl(PyObject *module, double x);

static PyObject *
math_modf(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_modf_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_log__doc__,
"log(x, [base=math.e])\n"
"Return the logarithm of x to the given base.\n"
"\n"
"If the base not specified, returns the natural logarithm (base e) of x.");

#define MATH_LOG_METHODDEF    \
    {"log", (PyCFunction)math_log, METH_VARARGS, math_log__doc__},

static PyObject *
math_log_impl(PyObject *module, PyObject *x, int group_right_1,
              PyObject *base);

static PyObject *
math_log(PyObject *module, PyObject *args)
{
    PyObject *return_value = NULL;
    PyObject *x;
    int group_right_1 = 0;
    PyObject *base = NULL;

    switch (PyTuple_GET_SIZE(args)) {
        case 1:
            if (!PyArg_ParseTuple(args, "O:log", &x)) {
                goto exit;
            }
            break;
        case 2:
            if (!PyArg_ParseTuple(args, "OO:log", &x, &base)) {
                goto exit;
            }
            group_right_1 = 1;
            break;
        default:
            PyErr_SetString(PyExc_TypeError, "math.log 需要 1 到 2 个参数");
            goto exit;
    }
    return_value = math_log_impl(module, x, group_right_1, base);

exit:
    return return_value;
}

PyDoc_STRVAR(math_log2__doc__,
"log2($module, x, /)\n"
"--\n"
"\n"
"Return the base 2 logarithm of x.");

#define MATH_LOG2_METHODDEF    \
    {"log2", (PyCFunction)math_log2, METH_O, math_log2__doc__},

PyDoc_STRVAR(math_log10__doc__,
"log10($module, x, /)\n"
"--\n"
"\n"
"Return the base 10 logarithm of x.");

#define MATH_LOG10_METHODDEF    \
    {"log10", (PyCFunction)math_log10, METH_O, math_log10__doc__},

PyDoc_STRVAR(math_fmod__doc__,
"fmod($module, x, y, /)\n"
"--\n"
"\n"
"Return fmod(x, y), according to platform C.\n"
"\n"
"x % y may differ.");

#define MATH_FMOD_METHODDEF    \
    {"fmod", (PyCFunction)(void(*)(void))math_fmod, METH_FASTCALL, math_fmod__doc__},

PyDoc_STRVAR(math_浮点模__doc__,
"浮点模($module, x, y, /)\n"
"--\n"
"\n"
"返回 x/y 的浮点余数, 根据平台 C 语言,\n"
"\n"
"x % y 的结果可能与此不同.");

#define MATH_浮点模_METHODDEF    \
    {"浮点模", (PyCFunction)(void(*)(void))math_fmod, METH_FASTCALL, math_浮点模__doc__},

static PyObject *
math_fmod_impl(PyObject *module, double x, double y);

static PyObject *
math_fmod(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    double x;
    double y;

    if (!_PyArg_CheckPositional("fmod", nargs, 2, 2)) {
        goto exit;
    }
    if (PyFloat_CheckExact(args[0])) {
        x = PyFloat_AS_DOUBLE(args[0]);
    }
    else
    {
        x = PyFloat_AsDouble(args[0]);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    if (PyFloat_CheckExact(args[1])) {
        y = PyFloat_AS_DOUBLE(args[1]);
    }
    else
    {
        y = PyFloat_AsDouble(args[1]);
        if (y == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_fmod_impl(module, x, y);

exit:
    return return_value;
}

PyDoc_STRVAR(math_dist__doc__,
"dist($module, p, q, /)\n"
"--\n"
"\n"
"Return the Euclidean distance between two points p and q.\n"
"\n"
"The points should be specified as sequences (or iterables) of\n"
"coordinates.  Both inputs must have the same dimension.\n"
"\n"
"Roughly equivalent to:\n"
"    sqrt(sum((px - qx) ** 2.0 for px, qx in zip(p, q)))");

#define MATH_DIST_METHODDEF    \
    {"dist", (PyCFunction)(void(*)(void))math_dist, METH_FASTCALL, math_dist__doc__},

PyDoc_STRVAR(math_距离__doc__,
"距离($module, p, q, /)\n"
"--\n"
"\n"
"返回两点 p 和 q 之间的欧氏距离。\n"
"\n"
"点应表示为坐标系列 (或可迭代对象).\n"
"两个输入的量纲必须相同.\n"
"\n"
"大致相当于:\n"
"    开方(和((px - qx) ** 2.0 取 px, qx 于 打包(p, q)))");

#define MATH_距离_METHODDEF    \
    {"距离", (PyCFunction)(void(*)(void))math_dist, METH_FASTCALL, math_距离__doc__},

static PyObject *
math_dist_impl(PyObject *module, PyObject *p, PyObject *q);

static PyObject *
math_dist(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *p;
    PyObject *q;

    if (!_PyArg_CheckPositional("dist", nargs, 2, 2)) {
        goto exit;
    }
    p = args[0];
    q = args[1];
    return_value = math_dist_impl(module, p, q);

exit:
    return return_value;
}

PyDoc_STRVAR(math_pow__doc__,
"pow($module, x, y, /)\n"
"--\n"
"\n"
"Return x**y (x to the power of y).");

#define MATH_POW_METHODDEF    \
    {"pow", (PyCFunction)(void(*)(void))math_pow, METH_FASTCALL, math_pow__doc__},

PyDoc_STRVAR(math_乘方__doc__,
"乘方($module, x, y, /)\n"
"--\n"
"\n"
"返回 x**y (x 的 y 次方).");

#define MATH_乘方_METHODDEF    \
    {"乘方", (PyCFunction)(void(*)(void))math_pow, METH_FASTCALL, math_乘方__doc__},

static PyObject *
math_pow_impl(PyObject *module, double x, double y);

static PyObject *
math_pow(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    double x;
    double y;

    if (!_PyArg_CheckPositional("pow", nargs, 2, 2)) {
        goto exit;
    }
    if (PyFloat_CheckExact(args[0])) {
        x = PyFloat_AS_DOUBLE(args[0]);
    }
    else
    {
        x = PyFloat_AsDouble(args[0]);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    if (PyFloat_CheckExact(args[1])) {
        y = PyFloat_AS_DOUBLE(args[1]);
    }
    else
    {
        y = PyFloat_AsDouble(args[1]);
        if (y == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_pow_impl(module, x, y);

exit:
    return return_value;
}

PyDoc_STRVAR(math_degrees__doc__,
"degrees($module, x, /)\n"
"--\n"
"\n"
"Convert angle x from radians to degrees.");

#define MATH_DEGREES_METHODDEF    \
    {"degrees", (PyCFunction)math_degrees, METH_O, math_degrees__doc__},

PyDoc_STRVAR(math_度数__doc__,
"度数($module, x, /)\n"
"--\n"
"\n"
"将角度 x 从弧度转换为度数.");

#define MATH_度数_METHODDEF    \
    {"度数", (PyCFunction)math_degrees, METH_O, math_度数__doc__},

static PyObject *
math_degrees_impl(PyObject *module, double x);

static PyObject *
math_degrees(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_degrees_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_radians__doc__,
"radians($module, x, /)\n"
"--\n"
"\n"
"Convert angle x from degrees to radians.");

#define MATH_RADIANS_METHODDEF    \
    {"radians", (PyCFunction)math_radians, METH_O, math_radians__doc__},

PyDoc_STRVAR(math_弧度__doc__,
"弧度($module, x, /)\n"
"--\n"
"\n"
"将角度 x 从度数转换为弧度.");

#define MATH_弧度_METHODDEF    \
    {"弧度", (PyCFunction)math_radians, METH_O, math_弧度__doc__},

static PyObject *
math_radians_impl(PyObject *module, double x);

static PyObject *
math_radians(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_radians_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_isfinite__doc__,
"isfinite($module, x, /)\n"
"--\n"
"\n"
"Return True if x is neither an infinity nor a NaN, and False otherwise.");

#define MATH_ISFINITE_METHODDEF    \
    {"isfinite", (PyCFunction)math_isfinite, METH_O, math_isfinite__doc__},

PyDoc_STRVAR(math_是有限__doc__,
"是有限($module, x, /)\n"
"--\n"
"\n"
"如果 x 不是无穷大, 也不是 NaN(非数), 则返回 True, 否则返回 False.");

#define MATH_是有限_METHODDEF    \
    {"是有限", (PyCFunction)math_isfinite, METH_O, math_是有限__doc__},

static PyObject *
math_isfinite_impl(PyObject *module, double x);

static PyObject *
math_isfinite(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_isfinite_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_isnan__doc__,
"isnan($module, x, /)\n"
"--\n"
"\n"
"Return True if x is a NaN (not a number), and False otherwise.");

#define MATH_ISNAN_METHODDEF    \
    {"isnan", (PyCFunction)math_isnan, METH_O, math_isnan__doc__},

PyDoc_STRVAR(math_不是数__doc__,
"不是数($module, x, /)\n"
"--\n"
"\n"
"如果 x 不是数 (NaN), 则返回 True, 否则返回 False.");

#define MATH_不是数_METHODDEF    \
    {"不是数", (PyCFunction)math_isnan, METH_O, math_不是数__doc__},

static PyObject *
math_isnan_impl(PyObject *module, double x);

static PyObject *
math_isnan(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_isnan_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_isinf__doc__,
"isinf($module, x, /)\n"
"--\n"
"\n"
"Return True if x is a positive or negative infinity, and False otherwise.");

#define MATH_ISINF_METHODDEF    \
    {"isinf", (PyCFunction)math_isinf, METH_O, math_isinf__doc__},

PyDoc_STRVAR(math_是无穷__doc__,
"是无穷($module, x, /)\n"
"--\n"
"\n"
"如果 x 是正无穷或负无穷, 则返回 True, 否则返回 False.");

#define MATH_是无穷_METHODDEF    \
    {"是无穷", (PyCFunction)math_isinf, METH_O, math_是无穷__doc__},

static PyObject *
math_isinf_impl(PyObject *module, double x);

static PyObject *
math_isinf(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double x;

    if (PyFloat_CheckExact(arg)) {
        x = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        x = PyFloat_AsDouble(arg);
        if (x == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = math_isinf_impl(module, x);

exit:
    return return_value;
}

PyDoc_STRVAR(math_isclose__doc__,
"isclose($module, /, a, b, *, rel_tol=1e-09, abs_tol=0.0)\n"
"--\n"
"\n"
"Determine whether two floating point numbers are close in value.\n"
"\n"
"  rel_tol\n"
"    maximum difference for being considered \"close\", relative to the\n"
"    magnitude of the input values\n"
"  abs_tol\n"
"    maximum difference for being considered \"close\", regardless of the\n"
"    magnitude of the input values\n"
"\n"
"Return True if a is close in value to b, and False otherwise.\n"
"\n"
"For the values to be considered close, the difference between them\n"
"must be smaller than at least one of the tolerances.\n"
"\n"
"-inf, inf and NaN behave similarly to the IEEE 754 Standard.  That\n"
"is, NaN is not close to anything, even itself.  inf and -inf are\n"
"only close to themselves.");

#define MATH_ISCLOSE_METHODDEF    \
    {"isclose", (PyCFunction)(void(*)(void))math_isclose, METH_FASTCALL|METH_KEYWORDS, math_isclose__doc__},

PyDoc_STRVAR(math_是接近__doc__,
"是接近($module, /, a, b, *, 相对误差=1e-09, 绝对误差=0.0)\n"
"--\n"
"\n"
"判断两个浮点数的值是否接近.\n"
"\n"
"  相对误差\n"
"    判断是否 \"接近\" 的最大差值, 相对于\n"
"    输入值的大小\n"
"  绝对误差\n"
"    判断是否 \"接近\" 的最大差值, 不考虑\n"
"    输入值的大小\n"
"\n"
"如果 a 值接近 b 值, 则返回 True, 否则返回 False.\n"
"\n"
"认为接近的两个值之差\n"
"必须小于至少一个给定的误差.\n"
"\n"
"负无穷/正无穷/非数 (NaN) 的接近判断规则类似于 IEEE 754 标准.\n"
"NaN 不接近于任何东西, 包括其自身.  正/负无穷\n"
"仅接近于其自身.");

#define MATH_是接近_METHODDEF    \
    {"是接近", (PyCFunction)(void(*)(void))math_是接近, METH_FASTCALL|METH_KEYWORDS, math_是接近__doc__},

static int
math_isclose_impl(PyObject *module, double a, double b, double rel_tol,
                  double abs_tol);

static PyObject *
math_isclose(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"a", "b", "rel_tol", "abs_tol", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "isclose", 0};
    PyObject *argsbuf[4];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    double a;
    double b;
    double rel_tol = 1e-09;
    double abs_tol = 0.0;
    int _return_value;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (PyFloat_CheckExact(args[0])) {
        a = PyFloat_AS_DOUBLE(args[0]);
    }
    else
    {
        a = PyFloat_AsDouble(args[0]);
        if (a == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    if (PyFloat_CheckExact(args[1])) {
        b = PyFloat_AS_DOUBLE(args[1]);
    }
    else
    {
        b = PyFloat_AsDouble(args[1]);
        if (b == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    if (args[2]) {
        if (PyFloat_CheckExact(args[2])) {
            rel_tol = PyFloat_AS_DOUBLE(args[2]);
        }
        else
        {
            rel_tol = PyFloat_AsDouble(args[2]);
            if (rel_tol == -1.0 && PyErr_Occurred()) {
                goto exit;
            }
        }
        if (!--noptargs) {
            goto skip_optional_kwonly;
        }
    }
    if (PyFloat_CheckExact(args[3])) {
        abs_tol = PyFloat_AS_DOUBLE(args[3]);
    }
    else
    {
        abs_tol = PyFloat_AsDouble(args[3]);
        if (abs_tol == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
skip_optional_kwonly:
    _return_value = math_isclose_impl(module, a, b, rel_tol, abs_tol);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyBool_FromLong((long)_return_value);

exit:
    return return_value;
}

static PyObject *
math_是接近(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"a", "b", "相对误差", "绝对误差", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "是接近", 0};
    PyObject *argsbuf[4];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    double a;
    double b;
    double rel_tol = 1e-09;
    double abs_tol = 0.0;
    int _return_value;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (PyFloat_CheckExact(args[0])) {
        a = PyFloat_AS_DOUBLE(args[0]);
    }
    else
    {
        a = PyFloat_AsDouble(args[0]);
        if (a == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    if (PyFloat_CheckExact(args[1])) {
        b = PyFloat_AS_DOUBLE(args[1]);
    }
    else
    {
        b = PyFloat_AsDouble(args[1]);
        if (b == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    if (args[2]) {
        if (PyFloat_CheckExact(args[2])) {
            rel_tol = PyFloat_AS_DOUBLE(args[2]);
        }
        else
        {
            rel_tol = PyFloat_AsDouble(args[2]);
            if (rel_tol == -1.0 && PyErr_Occurred()) {
                goto exit;
            }
        }
        if (!--noptargs) {
            goto skip_optional_kwonly;
        }
    }
    if (PyFloat_CheckExact(args[3])) {
        abs_tol = PyFloat_AS_DOUBLE(args[3]);
    }
    else
    {
        abs_tol = PyFloat_AsDouble(args[3]);
        if (abs_tol == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
skip_optional_kwonly:
    _return_value = math_isclose_impl(module, a, b, rel_tol, abs_tol);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyBool_FromLong((long)_return_value);

exit:
    return return_value;
}

PyDoc_STRVAR(math_prod__doc__,
"prod($module, iterable, /, *, start=1)\n"
"--\n"
"\n"
"Calculate the product of all the elements in the input iterable.\n"
"\n"
"The default start value for the product is 1.\n"
"\n"
"When the iterable is empty, return the start value.  This function is\n"
"intended specifically for use with numeric values and may reject\n"
"non-numeric types.");

#define MATH_PROD_METHODDEF    \
    {"prod", (PyCFunction)(void(*)(void))math_prod, METH_FASTCALL|METH_KEYWORDS, math_prod__doc__},

PyDoc_STRVAR(math_乘积__doc__,
"乘积($module, 可迭代对象, /, *, 起始=1)\n"
"--\n"
"\n"
"计算输入可迭代对象中所有元素的乘积.\n"
"\n"
"乘积的默认起始值为 1.\n"
"\n"
"如果可迭代对象为空, 返回起始值.  \n"
"此函数仅适用于数字值, 可能会拒绝\n"
"非数字类型.");

#define MATH_乘积_METHODDEF    \
    {"乘积", (PyCFunction)(void(*)(void))math_乘积, METH_FASTCALL|METH_KEYWORDS, math_乘积__doc__},

static PyObject *
math_prod_impl(PyObject *module, PyObject *iterable, PyObject *start);

static PyObject *
math_prod(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"", "start", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "prod", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *iterable;
    PyObject *start = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    iterable = args[0];
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    start = args[1];
skip_optional_kwonly:
    return_value = math_prod_impl(module, iterable, start);

exit:
    return return_value;
}

static PyObject *
math_乘积(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"", "起始", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "乘积", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *iterable;
    PyObject *start = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    iterable = args[0];
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    start = args[1];
skip_optional_kwonly:
    return_value = math_prod_impl(module, iterable, start);

exit:
    return return_value;
}

PyDoc_STRVAR(math_perm__doc__,
"perm($module, n, k=None, /)\n"
"--\n"
"\n"
"Number of ways to choose k items from n items without repetition and with order.\n"
"\n"
"Evaluates to n! / (n - k)! when k <= n and evaluates\n"
"to zero when k > n.\n"
"\n"
"If k is not specified or is None, then k defaults to n\n"
"and the function returns n!.\n"
"\n"
"Raises TypeError if either of the arguments are not integers.\n"
"Raises ValueError if either of the arguments are negative.");

#define MATH_PERM_METHODDEF    \
    {"perm", (PyCFunction)(void(*)(void))math_perm, METH_FASTCALL, math_perm__doc__},

PyDoc_STRVAR(math_排列__doc__,
"排列($module, n, k=空, /)\n"
"--\n"
"\n"
"从 n 个元素中取 k 个元素的方法数, 无重复, 考虑顺序.\n"
"\n"
"当 k<= n 时, 结果等于 n! / (n - k)!; 当 k > n 时,\n"
"结果为 0.\n"
"\n"
"如果 k 未指定或为空, 则 k 默认为 n,\n"
"函数返回 n!.\n"
"\n"
"Raises TypeError if either of the arguments are not integers.\n"
"Raises ValueError if either of the arguments are negative.");

#define MATH_排列_METHODDEF    \
    {"排列", (PyCFunction)(void(*)(void))math_perm, METH_FASTCALL, math_排列__doc__},

static PyObject *
math_perm_impl(PyObject *module, PyObject *n, PyObject *k);

static PyObject *
math_perm(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *n;
    PyObject *k = Py_None;

    if (!_PyArg_CheckPositional("perm", nargs, 1, 2)) {
        goto exit;
    }
    n = args[0];
    if (nargs < 2) {
        goto skip_optional;
    }
    k = args[1];
skip_optional:
    return_value = math_perm_impl(module, n, k);

exit:
    return return_value;
}

PyDoc_STRVAR(math_comb__doc__,
"comb($module, n, k, /)\n"
"--\n"
"\n"
"Number of ways to choose k items from n items without repetition and without order.\n"
"\n"
"Evaluates to n! / (k! * (n - k)!) when k <= n and evaluates\n"
"to zero when k > n.\n"
"\n"
"Also called the binomial coefficient because it is equivalent\n"
"to the coefficient of k-th term in polynomial expansion of the\n"
"expression (1 + x)**n.\n"
"\n"
"Raises TypeError if either of the arguments are not integers.\n"
"Raises ValueError if either of the arguments are negative.");

#define MATH_COMB_METHODDEF    \
    {"comb", (PyCFunction)(void(*)(void))math_comb, METH_FASTCALL, math_comb__doc__},

PyDoc_STRVAR(math_组合__doc__,
"组合($module, n, k, /)\n"
"--\n"
"\n"
"从 n 个元素中取 k 个元素的方法数, 无重复, 不考虑顺序.\n"
"\n"
"当 k <= n 时, 结果等于 n! / (k! * (n - k)!); 当 k > n 时,\n"
"结果为 0.\n"
"\n"
"Also called the binomial coefficient because it is equivalent\n"
"to the coefficient of k-th term in polynomial expansion of the\n"
"expression (1 + x)**n.\n"
"\n"
"Raises TypeError if either of the arguments are not integers.\n"
"Raises ValueError if either of the arguments are negative.");

#define MATH_组合_METHODDEF    \
    {"组合", (PyCFunction)(void(*)(void))math_comb, METH_FASTCALL, math_组合__doc__},

static PyObject *
math_comb_impl(PyObject *module, PyObject *n, PyObject *k);

static PyObject *
math_comb(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *n;
    PyObject *k;

    if (!_PyArg_CheckPositional("comb", nargs, 2, 2)) {
        goto exit;
    }
    n = args[0];
    k = args[1];
    return_value = math_comb_impl(module, n, k);

exit:
    return return_value;
}
/*[clinic end generated code: output=9a2b3dc91eb9aadd input=a9049054013a1b77]*/
